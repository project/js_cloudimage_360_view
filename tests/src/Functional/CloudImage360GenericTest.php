<?php

namespace Drupal\Tests\js_cloudimage_360_view\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for js_cloudimage_360_view.
 *
 * @group js_cloudimage_360_view
 */
class CloudImage360GenericTest extends GenericModuleTestBase {

  /**
   * {@inheritDoc}
   */
  protected function assertHookHelp(string $module): void {
    // Don't do anything here. Just overwrite this useless method, so we do
    // don't have to implement hook_help().
  }

}
