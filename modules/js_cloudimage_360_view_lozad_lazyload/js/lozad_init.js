/**
 * @file
 * JS Cloudimage 360 view behaviors.
 */
(function (once) {
  Drupal.behaviors.jsCloudimage360ViewLozadInit = {
    attach(context, settings) {
      once('lozad-init', 'body').forEach((element) => {
        const lozadObserver = lozad();
        lozadObserver.observe();
      });
    },
  };
})(once);
