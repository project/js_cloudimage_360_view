# JS Cloudimage 360 view module

The "JS Cloudimage 360 view module" provides an image field formatter. Which enables the user to display 360° views
of products and more on their page.

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

- The JS Cloudimage 360° view requires the [js-cloudimage-360-view](https://github.com/Scaleflex/js-cloudimage-360-view) library.
- Manually download the `3.2.0` release [here](https://github.com/scaleflex/js-cloudimage-360-view/archive/refs/tags/v3.2.0.zip), extract it, rename the `js-cloudimage-360-view-3.2.0` folder to `js-cloudimage-360-view` and put it in your libraries folder.
  - Make sure the path is as follows: `/libraries/js-cloudimage-360-view/build/js-cloudimage-360-view.min.js`.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

- Create an image field on an entity type bundle of your choice or use a pre-existing image field.
  - Make sure the cardinality of the field is set to `unlimited` or at least more than 10
  (otherwise the 360° view will look choppy).
- Go to the `Manage display` section of the entity and select the "Cloudimage 360 view" formatter for your image field.
- Here you can also adjust the display settings of the formatter as desired.
- Now when you create a new entity you can upload multiple pictures on the image field and the
formatter will generate a 360° view using the pictures you uploaded.