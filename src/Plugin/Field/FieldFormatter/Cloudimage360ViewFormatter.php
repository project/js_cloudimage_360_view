<?php

declare(strict_types=1);

namespace Drupal\js_cloudimage_360_view\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Template\Attribute;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Cloudimage 360 view' formatter.
 *
 * @FieldFormatter(
 *   id = "js_cloudimage_360_view",
 *   label = @Translation("Cloudimage 360 view"),
 *   field_types = {"image"},
 * )
 */
class Cloudimage360ViewFormatter extends ImageFormatter {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs an ImageFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityStorageInterface $image_style_storage
   *   The image style storage.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountInterface $current_user, EntityStorageInterface $image_style_storage, FileUrlGeneratorInterface $file_url_generator, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $current_user, $image_style_storage, $file_url_generator);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('entity_type.manager')->getStorage('image_style'),
      $container->get('file_url_generator'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'rotation_direction' => 'horizontally',
      'enable_control' => FALSE,
      'enable_key_control' => 'disabled',
      'enable_autoplay' => 'disabled',
      'enable_autoplay_reverse' => FALSE,
      'enable_fullscreen' => FALSE,
      'autoplay_speed' => 150,
      'drag_speed' => 150,
      'magnifier' => 1,
      'hide_360_logo' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $elements = parent::settingsForm($form, $form_state);
    unset($elements['image_link'], $elements['image_loading']);

    // @codingStandardsIgnoreStart
    // @todo Changing the rotation direction from horizontally is currently
    // not supported.
    // $elements['rotation_direction'] = [
    //   '#type' => 'select',
    //   '#title' => $this->t('Rotation direction'),
    //   '#description' => $this->t('The direction in which the image should be rotatable.'),
    //   '#options' => [
    //     'horizontally' => $this->t('Horizontally'),
    //     'vertically' => $this->t('Vertically'),
    //   ],
    //   '#default_value' => $this->getSetting('rotation_direction'),
    // ];
    // @codingStandardsIgnoreEnd
    $elements['enable_control'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable control'),
      '#description' => $this->t('Displays control arrows, which can be used to rotate the image.'),
      '#default_value' => $this->getSetting('enable_control'),
    ];
    $elements['enable_key_control'] = [
      '#type' => 'select',
      '#title' => $this->t('Enable key control'),
      '#description' => $this->t('Control the 360° view by pressing the arrow keys on your keyboard. "Enabled (in reverse)" will rotate the image in the opposite direction.'),
      '#options' => [
        'enabled' => $this->t('Enabled'),
        'reverse' => $this->t('Enabled (in reverse)'),
        'disabled' => $this->t('Disabled'),
      ],
      '#default_value' => $this->getSetting('enable_key_control'),
    ];

    $elements['enable_autoplay'] = [
      '#type' => 'select',
      '#title' => $this->t('Enable autoplay'),
      '#description' => $this->t('Automatically rotate the image without user interaction. "Enabled (rotate once)" will stop the autoplay after one complete cycle'),
      '#options' => [
        'enabled' => $this->t('Enabled'),
        'once' => $this->t('Enabled (rotate once)'),
        'disabled' => $this->t('Disabled'),
      ],
      '#default_value' => $this->getSetting('enable_autoplay'),
    ];
    $elements['hide_360_logo'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide 360° logo'),
      '#description' => $this->t('Hides the 360° view icon. Note that this will also hide the arrow controls until <a href="https://github.com/scaleflex/js-cloudimage-360-view/issues/166" target="_blank">this library bug</a> is solved.'),
      '#default_value' => $this->getSetting('hide_360_logo'),
      '#states' => [
        'visible' => [
          ':input[name*="[settings][enable_autoplay]"]' => ['value' => 'disabled'],
        ],
      ],
    ];
    $elements['enable_autoplay_reverse'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable autoplay reverse'),
      '#description' => $this->t('Reverse the autoplay direction'),
      '#default_value' => $this->getSetting('enable_autoplay_reverse'),
      '#states' => [
        'invisible' => [
          ':input[name*="[settings][enable_autoplay]"]' => ['value' => 'disabled'],
        ],
      ],
    ];
    $elements['enable_fullscreen'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable fullscreen'),
      '#description' => $this->t('Displays a "screen icon" which can be used to view the displayed model in fullscreen.'),
      '#default_value' => $this->getSetting('enable_fullscreen'),
    ];
    $elements['autoplay_speed'] = [
      '#type' => 'number',
      '#title' => $this->t('Autoplay speed'),
      '#description' => $this->t('The speed of changing frames for autoplay in milliseconds. Default: "150".'),
      '#default_value' => $this->getSetting('autoplay_speed'),
      '#states' => [
        'invisible' => [
          ':input[name*="[settings][enable_autoplay]"]' => ['value' => 'disabled'],
        ],
      ],
    ];
    $elements['drag_speed'] = [
      '#type' => 'number',
      '#title' => $this->t('Drag speed'),
      '#description' => $this->t('The speed factor of changing frames for drag in milliseconds. Default: "150".'),
      '#default_value' => $this->getSetting('drag_speed'),
    ];
    $elements['magnifier'] = [
      '#type' => 'number',
      '#title' => $this->t('Magnifier'),
      '#min' => 1,
      '#step' => 0.1,
      '#description' => $this->t('Displays a magnifying glass which can be used, to zoom in on the picture. The value is the zoom factor. Select "1" to disable the magnifier.'),
      '#default_value' => $this->getSetting('magnifier'),
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = [];

    $image_styles = image_style_options(FALSE);
    // Unset possible 'No defined styles' option.
    unset($image_styles['']);
    // Styles could be lost because of enabled/disabled modules that defines
    // their styles in code.
    $image_style_setting = $this->getSetting('image_style');
    if (isset($image_styles[$image_style_setting])) {
      $summary[] = $this->t('Image style: @style', ['@style' => $image_styles[$image_style_setting]]);
    }
    else {
      $summary[] = $this->t('Original image');
    }
    $summary[] = $this->t('Rotation direction: @direction', ['@direction' => $this->getSetting('rotation_direction')]);
    $summary[] = $this->t('Enable controls: @enable_control', ['@enable_control' => $this->getSetting('enable_control') ? $this->t('Enabled') : $this->t('Disabled')]);
    $summary[] = $this->t('Enable key control: @key_control', ['@key_control' => $this->getSetting('enable_key_control')]);
    $summary[] = $this->t('Enable autoplay: @autoplay', ['@autoplay' => $this->getSetting('enable_autoplay')]);
    if ($this->getSetting('enable_autoplay') !== 'disabled') {
      $summary[] = $this->t('Enable autoplay reverse: @autoplay_reverse', ['@autoplay_reverse' => $this->getSetting('enable_autoplay_reverse') ? $this->t('Enabled') : $this->t('Disabled')]);
      $summary[] = $this->t('Autoplay speed: @autoplay_speed', ['@autoplay_speed' => $this->getSetting('autoplay_speed')]);
    }
    $summary[] = $this->t('Enable fullscreen: @fullscreen', ['@fullscreen' => $this->getSetting('enable_fullscreen') ? $this->t('Enabled') : $this->t('Disabled')]);
    $summary[] = $this->t('Drag speed: @drag_speed', ['@drag_speed' => $this->getSetting('drag_speed')]);
    $summary[] = empty($this->getSetting('magnifier')) ? $this->t('Magnifier zoom: Disabled') : $this->t('Magnifier zoom: x@magnifier', ['@magnifier' => $this->getSetting('magnifier')]);
    $summary[] = $this->t('Hide 360° logo: @hide_logo', ['@hide_logo' => $this->getSetting('hide_360_logo') ? $this->t('Enabled') : $this->t('Disabled')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = parent::viewElements($items, $langcode);
    $firstElementKey = array_key_first($elements);
    $imageStyleId = $this->getSetting('image_style');
    $processedImageUrls = [];
    $cacheTags = NULL;
    $imageStyle = $this->entityTypeManager->getStorage('image_style')->load($imageStyleId);

    foreach ($elements as $delta => $element) {
      // The cache tags are identical for each element, so just get them once:
      if ($delta === $firstElementKey && !empty($element['#cache']['tags'])) {
        $cacheTags = $element['#cache']['tags'];
      }
      // Continue if no image item is set:
      if (!isset($element['#item'])) {
        continue;
      }
      $imageUri = $element['#item']->entity->getFileUri();
      $imageUrl = $this->fileUrlGenerator->generateAbsoluteString($imageUri);

      $processedImageUrl = $imageStyle ? $imageStyle->buildUrl($imageUri) : $imageUrl;
      if (!empty($processedImageUrl)) {
        $processedImageUrls[] = $processedImageUrl;
      }
    }
    $imageUrlJson = json_encode($processedImageUrls, JSON_UNESCAPED_SLASHES);

    // Get the display settings:
    $displaySettings = $this->getSettings();
    // Create a new attributes object:
    $attributes = $this->createThemeAttributes($displaySettings, $imageUrlJson);

    return [
      '#theme' => 'js_cloudimage_360_view',
      '#file_urls_string' => $imageUrlJson,
      '#display_settings' => $displaySettings,
      '#attributes' => $attributes,
      '#cache' => [
        'tags' => $cacheTags,
      ],
      '#attached' => [
        'library' => [
          'js_cloudimage_360_view/js_cloudimage_360_view',
        ],
      ],
    ];
  }

  /**
   * Creates theme attributes based on the display settings and image URLs.
   *
   * @param array $displaySettings
   *   The display settings of the field formatter.
   * @param string $imageUrlsString
   *   The string containing the image URLs.
   *
   * @return \Drupal\Core\Template\Attribute|array
   *   An attribute object generated based on the display settings or an empty
   *   array if no image URLs are set.
   */
  protected function createThemeAttributes(array $displaySettings, string $imageUrlsString): Attribute|array {
    // If no image URLs are set, return an empty array:
    if (empty($imageUrlsString)) {
      return [];
    }
    $imagesPath = \Drupal::service('extension.list.module')->getPath('js_cloudimage_360_view') . '/images';
    $attributes = new Attribute(['class' => ['cloudimage-360']]);

    // Switch to the locally stored 360° image instead the default CDN image:
    $attributes->setAttribute('logo-src', $imagesPath . '360_view.svg');

    if ($displaySettings['rotation_direction'] === 'horizontally') {
      $attributes->setAttribute('data-image-list-x', $imageUrlsString);
    }
    else {
      $attributes->setAttribute('data-image-list-y', $imageUrlsString);
      if ($displaySettings['enable_autoplay'] !== 'disabled') {
        // If the rotation direction is vertical and autoplay is enabled, set
        // the autoplay behavior to spin vertically:
        $attributes->setAttribute('data-autoplay-behavior', 'spin-y');
      }
    }
    if ($displaySettings['enable_control']) {
      $attributes->setAttribute('data-control', TRUE);
    }
    if ($displaySettings['enable_fullscreen']) {
      $attributes->setAttribute('data-fullscreen', TRUE);
    }

    if ($displaySettings['enable_key_control'] !== 'disabled') {
      $attributes->setAttribute('data-keys', TRUE);
      if ($displaySettings['enable_key_control'] === 'reverse') {
        $attributes->setAttribute('data-keys-reverse', TRUE);
      }
    }

    if ($displaySettings['enable_autoplay'] !== 'disabled') {
      $attributes->setAttribute('data-autoplay', TRUE);
      if ($displaySettings['enable_autoplay'] === 'once') {
        $attributes->setAttribute('data-play-once', TRUE);
      }
      if ($displaySettings['enable_autoplay_reverse']) {
        $attributes->setAttribute('data-autoplay-reverse', TRUE);
      }
      $attributes->setAttribute('data-speed', $displaySettings['autoplay_speed']);
    }

    $attributes->setAttribute('data-drag-speed', $displaySettings['drag_speed']);

    if ($displaySettings['magnifier'] > 0) {
      $attributes->setAttribute('data-magnifier', $displaySettings['magnifier']);
    }

    if ($displaySettings['hide_360_logo'] && $displaySettings['enable_autoplay'] === 'disabled') {
      $attributes->setAttribute('data-hide-360-logo', TRUE);
    }

    $imagesPath = base_path() . \Drupal::service('extension.list.module')->getPath('js_cloudimage_360_view') . '/images';

    // Manually add the 360 logo to the attributes, so we don't use the one
    // rendered through cdn:
    $logoPath = $imagesPath . '/360_view.svg';
    $attributes->setAttribute('logo-src', $logoPath);

    return $attributes;
  }

}
